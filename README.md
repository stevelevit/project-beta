# CarCar

CarCar is an application for managing the aspects of an automobile dealership. It manages the inventory, automobile sales, and automobile services.

Team:

* Steve Levit - Auto Sales
* Elliott Porter - Auto Services

## Getting Started

**Make sure you have Docker, Git, and Node.js 18.2 or above**

1. Fork this repository

https://gitlab.com/stevelevit/project-beta

2. Clone the forked repository onto your local computer:
git clone <<https://gitlab.com/stevelevit/project-beta.git>>

3. Build and run the project using Docker with these commands:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- After running these commands, make sure all of your Docker containers are running

- View the project in the browser: http://localhost:3000/

## Design

For the design of the website we implemented various styling components from bootstrap.

URLs:

http://localhost:3000/technicians/create
http://localhost:3000/technicians
http://localhost:3000/appointments/create
http://localhost:3000/appointments
http://localhost:3000/servicehistory
http://localhost:3000/manufacturers
http://localhost:3000/manufacturers/create
http://localhost:3000/models
http://localhost:3000/models/create
http://localhost:3000/automobiles
http://localhost:3000/automobiles/create


API DOCUMENTATION

Manufacturers:

List manufacturers    GET    http://localhost:8100/api/manufacturers/
Create a manufacturer    POST    http://localhost:8100/api/manufacturers/
Get a specific manufacturer    GET    http://localhost:8100/api/manufacturers/:id/
Update a specific manufacturer    PUT    http://localhost:8100/api/manufacturers/:id/
Delete a specific manufacturer    DELETE    http://localhost:8100/api/manufacturers/:id/


Vehicle Models

List vehicle models    GET    http://localhost:8100/api/models/
Create a vehicle model    POST    http://localhost:8100/api/models/
Get a specific vehicle model    GET    http://localhost:8100/api/models/:id/
Update a specific vehicle model    PUT    http://localhost:8100/api/models/:id/
Delete a specific vehicle model    DELETE    http://localhost:8100/api/models/:id/

Automobile Information

List automobiles    GET    http://localhost:8100/api/automobiles/
Create an automobile    POST    http://localhost:8100/api/automobiles/
Get a specific automobile    GET    http://localhost:8100/api/automobiles/:vin/
Update a specific automobile    PUT    http://localhost:8100/api/automobiles/:vin/
Delete a specific automobile    DELETE    http://localhost:8100/api/automobiles/:vin/


AUTO SERVICE API Endpoints

List technicians    GET    http://localhost:8080/api/technicians/
Create a technician    POST    http://localhost:8080/api/technicians/
Delete a specific technician    DELETE    http://localhost:8080/api/technicians/:id/
List appointments    GET    http://localhost:8080/api/appointments/
Create an appointment    POST    http://localhost:8080/api/appointments/
Delete an appointment    DELETE    http://localhost:8080/api/appointments/:id/
Set appointment status to "canceled"    PUT    http://localhost:8080/api/appointments/:id/cancel/
Set appointment status to "finished"    PUT    http://localhost:8080/api/appointments/:id/finish/

Automobile Sales API Endpoints

List salespeople    GET    http://localhost:8090/api/salespeople/
Create a salesperson    POST    http://localhost:8090/api/salespeople/
Delete a specific salesperson    DELETE    http://localhost:8090/api/salespeople/:id/
List customers    GET    http://localhost:8090/api/customers/
Create a customer    POST    http://localhost:8090/api/customers/
Delete a specific customer    DELETE    http://localhost:8090/api/customers/:id/
List sales    GET    http://localhost:8090/api/sales/
Create a sale    POST    http://localhost:8090/api/sales/
Delete a sale    DELETE    http://localhost:8090/api/sales/:id

## Service microservice
The Service microservice augments the dealership's offerings by providing VIP services and managing service appointments. At the core of this service are the technicians, whose management is facilitated through endpoints for listing, detailing, creating, and removing technician records. Service appointments are meticulously handled, allowing for the scheduling, updating, and deletion of appointments. A unique feature is the service history tracking based on vehicle VIN, offering a detailed view of each vehicle's service interactions over time.

This comprehensive system architecture not only ensures efficient internal operations but also enhances the customer experience through streamlined processes and value-added services.

## Sales microservice

To interact with the system, several endpoints are provided across different domains such as manufacturers, vehicle models, and automobiles. These are accessible through user-friendly interfaces like Insomnia and standard web browsers. For manufacturers, the system supports a variety of actions including listing all manufacturers, creating new ones, fetching details for a specific manufacturer, updating existing records, and removing them as necessary. Similar functionalities extend to vehicle models, allowing users to manage and update model details, including images and associations with manufacturers. Automobile management is similarly robust, with endpoints dedicated to creating, listing, updating, and deleting automobiles, each uniquely identified by a VIN.

The Sales microservice is a key component that combines with the Inventory microservice. It automates the flow of data regarding sales transactions through a dedicated poller. This seamless integration ensures that sales records are always up to date with the latest inventory data. The microservice architecture facilitates comprehensive management of customers, salespeople, and sales records. It supports listing, creating, and deleting operations for customers and salespeople, alongside the capability to generate new sales records that tie together automobiles, customers, and sales personnel.
* Elliott - Service
* Stephen - Sales

## Design

Before initiating your project setup, confirm the installation of Docker, Git, and Node.js (minimum version 18.2). Begin by forking the repository of interest. Subsequently, clone the forked repository to your local system using the git clone <<repository.url.here>> command. To compile and initiate the project within Docker, employ the following commands:

docker volume create beta-data
docker-compose build
docker-compose up

Post-execution, verify the operation of all Docker containers and access the project via http://localhost:3000/.

CarCar is constructed around a three microservices: Inventory, Services, and Sales. These services share data dynamically, thanks to a poller mechanism that ensures the sales and service teams have access to the most current inventory information.

To interact with the system, several endpoints are provided across different domains such as manufacturers, vehicle models, and automobiles. These are accessible through user-friendly interfaces like Insomnia and standard web browsers. For manufacturers, the system supports a variety of actions including listing all manufacturers, creating new ones, fetching details for a specific manufacturer, updating existing records, and removing them as necessary. Similar functionalities extend to vehicle models, allowing users to manage and update model details, including images and associations with manufacturers. Automobile management is similarly robust, with endpoints dedicated to creating, listing, updating, and deleting automobiles, each uniquely identified by a VIN.


## Service microservice

The Service microservice augments the dealership's offerings by providing VIP services and managing service appointments. At the core of this service are the technicians, whose management is facilitated through endpoints for listing, detailing, creating, and removing technician records. Service appointments are meticulously handled, allowing for the scheduling, updating, and deletion of appointments. A unique feature is the service history tracking based on vehicle VIN, offering a detailed view of each vehicle's service interactions over time.



## Sales microservice


The Sales microservice is a key component that synergizes with the Inventory microservice. It automates the flow of data regarding sales transactions through a dedicated poller. This seamless integration ensures that sales records are always up to date with the latest inventory data. The microservice architecture facilitates comprehensive management of customers, salespeople, and sales records. It supports listing, creating, and deleting operations for customers and salespeople, alongside the capability to generate new sales records that tie together automobiles, customers, and sales personnel.

URLs

http://localhost:3000/technicians/create
http://localhost:3000/technicians
http://localhost:3000/appointments/create
http://localhost:3000/appointments
http://localhost:3000/servicehistory
http://localhost:3000/manufacturers
http://localhost:3000/manufacturers/create
http://localhost:3000/models
http://localhost:3000/models/create
http://localhost:3000/automobiles
http://localhost:3000/automobiles/create
