from django.urls import path
from . import views

urlpatterns = [
    path('customers/', views.customer_list, name='customer-list'),
    path('customers/<int:id>/', views.customer_detail, name='customer-detail'),
    path('sales/', views.sale_list, name='sale-list'),
    path('sales/<int:sale_id>/', views.sale_detail, name='sale-detail'),
    path('salespeople/', views.salesperson_list, name='salesperson-list'),
    path('salespeople/<int:pk>/', views.salesperson_detail, name='salesperson-detail'),
]
