from django.http import HttpResponseBadRequest, JsonResponse, HttpResponse
from django.views.decorators.http import require_http_methods
from .models import Salesperson, Customer, Sale, AutomobileVO
import json
from common.json import ModelEncoder


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id"
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number"
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold"
    ]

    def default(self, obj):
        if isinstance(obj, self.model):
            data = {prop: getattr(obj, prop) for prop in self.properties}
            return data
        return super().default(obj)



class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "automobile",
        "salesperson",
        "customer",
        "price"
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }


@require_http_methods(["GET", "POST"])
def customer_list(request):
    if request.method == 'GET':
        customers = Customer.objects.all()
        encoded_customers = [CustomerEncoder().encode(customer) for customer in customers]
        return JsonResponse({'customers': encoded_customers}, safe=False)

    elif request.method == 'POST':
        try:
            data = json.loads(request.body)
            customer = Customer.objects.create(**data)
            encoded_customer = CustomerEncoder().encode(customer)
            return JsonResponse(encoded_customer, safe=False)
        except Exception as e:
            return HttpResponse(status=400, content=str(e))


@require_http_methods(["GET", "PUT", "DELETE"])
def customer_detail(request, id):
    try:
        customer = Customer.objects.get(id=id)
    except Customer.DoesNotExist:
        return JsonResponse({"message": "Customer does not exist"}, status=404)

    if request.method == 'GET':
        encoded_customer = CustomerEncoder().encode(customer)
        return JsonResponse(encoded_customer, safe=False)

    elif request.method == 'PUT':
        try:
            data = json.loads(request.body)
            for key, value in data.items():
                setattr(customer, key, value)
            customer.save()
            encoded_customer = CustomerEncoder().encode(customer)
            return JsonResponse(encoded_customer, safe=False)
        except Exception as e:
            return HttpResponse(status=400, content=str(e))

    elif request.method == 'DELETE':
        customer.delete()
        return HttpResponse(status=204)

@require_http_methods(["GET", "POST"])
def sale_list(request):
    if request.method == 'GET':
        sales = Sale.objects.all()
        for sale in sales:
            sale.price = float(sale.price)
        return JsonResponse({'sales': list(sales)}, safe=False, encoder=SaleEncoder)

    elif request.method == 'POST':
        try:
            data = json.loads(request.body)
            automobile_vin = data.get('automobile_vin')
            salesperson_id = data.get('salesperson_id')
            customer_id = data.get('customer_id')
            price = data.get('price')

            try:
                automobile = AutomobileVO.objects.get(vin=automobile_vin)
            except AutomobileVO.DoesNotExist:
                return HttpResponseBadRequest("Failed to create sale: AutomobileVO with VIN {} does not exist".format(automobile_vin))

            if automobile.sold:
                return HttpResponseBadRequest("Failed to create sale: AutomobileVO with VIN {} has already been sold".format(automobile_vin))
            try:
                salesperson = Salesperson.objects.get(id=salesperson_id)
                customer = Customer.objects.get(id=customer_id)
            except (Salesperson.DoesNotExist, Customer.DoesNotExist):
                return HttpResponseBadRequest("Failed to create sale: Salesperson or Customer does not exist")

            sale = Sale.objects.create(
                automobile=automobile,
                salesperson=salesperson,
                customer=customer,
                price=price
            )


            automobile.sold = True
            automobile.save()


            sale.price = float(sale.price)

            return JsonResponse(SaleEncoder().encode(sale), status=201, safe=False)

        except Exception as e:
            return HttpResponseBadRequest("Failed to create sale: {}".format(str(e)))


@require_http_methods(['GET', 'PUT', 'DELETE'])
def sale_detail(request, sale_id):
    if request.method == 'GET':
        try:
            sale = Sale.objects.get(id=sale_id)
            sale_data = {
                "id": sale.id,
                "automobile": SaleEncoder().encode(sale.automobile.vin),
                "salesperson": SaleEncoder().encode(sale.salesperson.employee_id),
                "customer": SaleEncoder().encode(sale.customer.id),
                "price": float(sale.price),

            }
            return JsonResponse(sale_data)
        except Sale.DoesNotExist:
            return JsonResponse({"error": "Sale not found"}, status=404)
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=500)

    elif request.method == 'PUT':

        return JsonResponse({"error": "PUT method not implemented"}, status=501)

    elif request.method == 'DELETE':
        try:
            sale = Sale.objects.get(id=sale_id)
            sale.delete()
            return JsonResponse({"message": "Sale deleted successfully"})
        except Sale.DoesNotExist:
            return JsonResponse({"error": "Sale not found"}, status=404)
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=500)


@require_http_methods(["GET", "POST"])
def salesperson_list(request):
    if request.method == 'GET':
        salespeople = Salesperson.objects.all()
        return JsonResponse({'salespeople': list(salespeople)}, safe=False, encoder=SalespersonEncoder)

    elif request.method == 'POST':
        try:
            data = json.loads(request.body)
            salesperson = Salesperson.objects.create(**data)
            return JsonResponse(salesperson, safe=False, encoder=SalespersonEncoder)
        except Exception as e:
            return HttpResponse(status=400, content=str(e))

@require_http_methods(["GET", "PUT", "DELETE"])
def salesperson_detail(request, pk):
    try:
        salesperson = Salesperson.objects.get(id=pk)
    except Salesperson.DoesNotExist:
        return JsonResponse({"message": "Salesperson does not exist"}, status=404)

    if request.method == 'GET':
        return JsonResponse(salesperson, safe=False, encoder=SalespersonEncoder)

    elif request.method == 'PUT':
        try:
            data = json.loads(request.body)
            for key, value in data.items():
                setattr(salesperson, key, value)
            salesperson.save()
            return JsonResponse(salesperson, safe=False, encoder=SalespersonEncoder)
        except Exception as e:
            return HttpResponse(status=400, content=str(e))

    elif request.method == 'DELETE':
        salesperson.delete()
        return HttpResponse(status=204)