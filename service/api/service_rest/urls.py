from django.urls import path
from .views import manage_technicians, manage_appointments

urlpatterns = [
    path('technicians/', manage_technicians, name='list_technicians'),
    path('technicians/create/', manage_technicians, name='create_technician'),
    path('technicians/<int:id>/delete/', manage_technicians, name='delete_technician'),

    path('appointments/', manage_appointments, name='manage_appointments'),
    path('appointments/<int:id>/', manage_appointments, name='manage_appointments'),
]
