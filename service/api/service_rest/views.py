from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404
from .models import Technician, AutomobileVO, Appointment
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]

@require_http_methods(["GET", "POST", "DELETE", "PUT"])
@csrf_exempt
def manage_technicians(request, id=None):
    if request.method == "GET":
        technicians = Technician.objects.all()
        data = [{"id": tech.id, "first_name": tech.first_name, "last_name": tech.last_name, "employee_id": tech.employee_id} for tech in technicians]
        return JsonResponse(data, safe=False)

    elif request.method == "POST":
        try:
            data = json.loads(request.body)
        except json.JSONDecodeError:
            return JsonResponse({"error": "Invalid JSON payload"}, status=400)

        first_name = data.get("firstName")
        last_name = data.get("lastName")
        employee_id = data.get("employeeId")

        if not all([first_name, last_name, employee_id]):
            return JsonResponse({"error": "Missing required fields"}, status=400)

        new_technician = Technician.objects.create(
            first_name=first_name,
            last_name=last_name,
            employee_id=employee_id
        )

        return JsonResponse({"message": "Technician created successfully", "technician_id": new_technician.id})

    elif request.method == "DELETE":
        technician = get_object_or_404(Technician, id=id)
        technician.delete()
        return JsonResponse({"message": "Technician deleted successfully"})

@require_http_methods(["GET", "POST", "DELETE", "PUT"])
@csrf_exempt
def manage_appointments(request, id=None):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        data = [
            {
                "id": app.id,
                "date_time": app.date_time,
                "reason": app.reason,
                "status": app.status,
                "vin": app.vin,
                "customer": app.customer,
                "technician_id": app.technician.id if app.technician else None,
                "vip": app.vip
            }
            for app in appointments
        ]
        return JsonResponse(data, safe=False)

    elif request.method == "POST":
        try:
            data = json.loads(request.body)
        except json.JSONDecodeError:
            return JsonResponse({"error": "Invalid JSON payload"}, status=400)

        date_time = data.get("date_time")
        reason = data.get("reason")
        status = data.get("status")
        vin = data.get("vin")
        customer = data.get("customer")
        technician_id = data.get("technician_id")

        if not all([date_time, reason, status, vin, customer, technician_id]):
            return JsonResponse({"error": "Missing required fields"}, status=400)

        try:
            automobile = AutomobileVO.objects.get(vin=vin)
            is_vip = automobile.sold
        except AutomobileVO.DoesNotExist:
            is_vip = False

        new_appointment = Appointment.objects.create(
            date_time=date_time,
            reason=reason,
            status=status,
            vin=vin,
            customer=customer,
            technician_id=technician_id,
            vip=is_vip
        )

        return JsonResponse({"message": "Appointment created successfully", "appointment_id": new_appointment.id})

    elif request.method == "DELETE":
        appointment = get_object_or_404(Appointment, id=id)
        appointment.delete()
        return JsonResponse({"message": "Appointment deleted successfully"})

    elif request.method == "PUT":
        try:
            data = json.loads(request.body)
        except json.JSONDecodeError:
            return JsonResponse({"error": "Invalid JSON payload"}, status=400)

        status = data.get("status")

        if not status:
            return JsonResponse({"error": "Missing required fields"}, status=400)

        appointment = get_object_or_404(Appointment, id=id)
        appointment.status = status
        appointment.save()

        return JsonResponse({"message": "Appointment status updated successfully"})
