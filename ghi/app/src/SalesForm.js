import React, { useState, useEffect } from 'react';

function SaleForm() {
    const [autos, setAutomobiles] = useState([])
    const [salespeople, setSalespeople] = useState([])
    const [customers, setCustomers] = useState([])
    const [formData, setFormData] = useState({
        price: '',
        automobile: '',
        salesperson: '',
        customer: ''
    })


useEffect(() => {
    fetch('http://localhost:8090/api/salespeople/')
        .then(response => response.json())
        .then(data => setSalespeople(data));
}, []);


useEffect(() => {
    fetch('http://localhost:8090/api/customers/')
        .then(response => response.json())
        .then(data => setCustomers(data));
}, []);


useEffect(() => {
    fetch('http://localhost:8090/api/automobiles/')
        .then(response => response.json())
        .then(data => setAutomobiles(data.filter(auto => !auto.sold)));
}, []);

const handleSubmit = async (event) => {
    event.preventDefault();
    // const data = {
    //     salesperson: selectedSalesperson,
    //     customer: selectedCustomer,
    //     automobile: selectedAutomobile,
    //     price
    // };

    const salesUrl = 'http://localhost:8090/api/sales/';
    const fetchConfig = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const response = await fetch(salesUrl, fetchConfig);
    if (response.ok) {
        const newSale = await response.json();
        console.log(newSale);

        // setSelectedSalesperson('');
        // setSelectedCustomer('');
        // setSelectedAutomobile('');
        // setPrice('');
    }
};

const containerStyle = { color: '#003366' };
return (
    <div className="container" style={containerStyle}>
        <div className="row">
            <div className="offset-md-3 col-md-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new sale</h1>
                    <form onSubmit={handleSubmit} id="create-sale-form">
                        {/* Select salesperson */}
                        {/* Select customer */}
                        {/* Select automobile */}
                        {/* Input price */}
                        {/* The rest of the form follows the same pattern as ShoeForm */}
                        {/* Ensure you change the names and IDs to match the sales context */}

                        <button className="btn btn-primary" type="submit">Create Sale</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
);
}
// export default SalesForm;