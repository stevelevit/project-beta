import React, { useState } from 'react';

const AddVehicleModelForm = () => {
  const [formData, setFormData] = useState({
    name: '',
    picture_url: '',
    manufacturer_id: '',
  });

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();


    fetch('http://localhost:8100/api/models/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    })
      .then(response => response.json())
      .then(data => {
        console.log('Success:', data);
        // You may want to redirect or handle success in your application
      })
      .catch((error) => {
        console.error('Error:', error);
        // Handle error in your application
      });
  };

  return (
    <div className="container mt-4">
      <h2>Add New Vehicle Model</h2>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="name" className="form-label">Model Name</label>
          <input type="text" className="form-control" id="name" name="name" onChange={handleChange} required />
        </div>
        <div className="mb-3">
          <label htmlFor="picture_url" className="form-label">Picture URL</label>
          <input type="url" className="form-control" id="picture_url" name="picture_url" onChange={handleChange} required />
        </div>
        <div className="mb-3">
          <label htmlFor="manufacturer_id" className="form-label">Manufacturer ID</label>
          <input type="number" className="form-control" id="manufacturer_id" name="manufacturer_id" onChange={handleChange} required />
        </div>
        <button type="submit" className="btn btn-primary">Add Model</button>
      </form>
    </div>
  );
};

export default AddVehicleModelForm;
