import React, { useState } from 'react';

const ServiceHistory = () => {
  const [searchVin, setSearchVin] = useState('');
  const [appointments, setAppointments] = useState([]);

  const handleSearch = () => {

    fetch(`http://localhost:8080/api/appointments/?vin=${searchVin}`)
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then(data => {

        setAppointments(data);
      })
      .catch(error => {
        console.error('Error fetching service history:', error);
      });
  };

  return (
    <div>
      <h1>Service History</h1>
      <div>
        <label htmlFor="vin">Enter VIN:</label>
        <input
          type="text"
          id="vin"
          value={searchVin}
          onChange={(e) => setSearchVin(e.target.value)}
        />
        <button onClick={handleSearch}>Search</button>
      </div>
      <div>
        {appointments.map(appointment => (
          <div key={appointment.id}>
            <p>VIN: {appointment.vin}</p>
            <p>VIP: {appointment.vip ? 'Yes' : 'No'}</p>
            <p>Customer: {appointment.customer}</p>
            <p>Date and Time: {appointment.date_time}</p>
            <p>Technician: {appointment.technician_id}</p>
            <p>Reason: {appointment.reason}</p>
            <p>Status: {appointment.status}</p>
            <hr />
          </div>
        ))}
      </div>
    </div>
  );
};

export default ServiceHistory;
