import React, { useState } from 'react';

const AddAutomobileForm = () => {
  const [formData, setFormData] = useState({
    color: '',
    year: '',
    vin: '',
    model_id: '',
  });

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const postData = {
      color: formData.color,
      year: parseInt(formData.year),
      vin: formData.vin,
      model_id: parseInt(formData.model_id),
    };

    fetch('http://localhost:8100/api/automobiles/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(postData),
    })
      .then(response => response.json())
      .then(data => {
        console.log('Success:', data);
        window.alert('Automobile submitted successfully!');
      })
      .catch((error) => {
        console.error('Error:', error);
        window.alert('Failed to submit automobile. Please try again.');
      });
  };

  return (
    <div className="container mt-4">
      <h2>Add New Automobile</h2>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="color" className="form-label">Color</label>
          <input type="text" className="form-control" id="color" name="color" onChange={handleChange} required />
        </div>
        <div className="mb-3">
          <label htmlFor="year" className="form-label">Year</label>
          <input type="number" className="form-control" id="year" name="year" onChange={handleChange} required />
        </div>
        <div className="mb-3">
          <label htmlFor="vin" className="form-label">VIN</label>
          <input type="text" className="form-control" id="vin" name="vin" onChange={handleChange} required />
        </div>
        <div className="mb-3">
          <label htmlFor="model_id" className="form-label">Model ID</label>
          <input type="number" className="form-control" id="model_id" name="model_id" onChange={handleChange} required />
        </div>
        <button type="submit" className="btn btn-primary">Add Automobile</button>
      </form>
    </div>
  );
};

export default AddAutomobileForm;
