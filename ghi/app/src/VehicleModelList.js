import React, { useState, useEffect } from 'react';

const VehicleModelList = () => {
  const [models, setModels] = useState([]);

  useEffect(() => {

    fetch('http://localhost:8100/api/models/')
      .then(response => response.json())
      .then(data => setModels(data.models))
      .catch(error => console.error(error));
  }, []);

  return (
    <div className="container mt-4">
      <h1>Vehicle Models</h1>
      <div className="row">
        {models.map(model => (
          <div key={model.id} className="col-md-4 mb-4">
            <div className="card">
              <img src={model.picture_url} className="card-img-top" alt={model.name} />
              <div className="card-body">
                <h5 className="card-title">{model.name}</h5>
                <p className="card-text">Manufacturer: {model.manufacturer.name}</p>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default VehicleModelList;
