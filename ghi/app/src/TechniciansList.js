import React, { useState, useEffect } from 'react';

const TechniciansList = () => {
  const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    fetch('http://localhost:8080/api/technicians/')
      .then(response => response.json())
      .then(data => setTechnicians(data))
      .catch(error => console.error(error));
  }, []);

  return (
    <div>
      <h1>Technicians</h1>
      <table className="table">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {technicians.map(tech => (
            <tr key={tech.id}>
              <td>{tech.employee_id}</td>
              <td>{tech.first_name}</td>
              <td>{tech.last_name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default TechniciansList;
