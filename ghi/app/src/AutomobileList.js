import React, { useState, useEffect } from 'react';

const AutomobileList = () => {
  const [automobiles, setAutomobiles] = useState([]);

  useEffect(() => {
    // Fetch the list of automobiles from the API
    fetch('http://localhost:8100/api/automobiles/')
      .then(response => response.json())
      .then(data => setAutomobiles(data.autos))
      .catch(error => console.error('Error:', error));
  }, []);

  return (
    <div className="container mt-4">
      <h2>Automobile List</h2>
      <table className="table">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Sold</th>
            <th>Model</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map(auto => (
            <tr key={auto.vin}>
              <td>{auto.vin}</td>
              <td>{auto.color}</td>
              <td>{auto.year}</td>
              <td>{auto.sold ? 'Yes' : 'No'}</td>
              <td>{auto.model.name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default AutomobileList;
