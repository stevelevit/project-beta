import React, { useState } from 'react';

const ServiceAppointmentForm = () => {
  const [formData, setFormData] = useState({
    vin: '',
    customer: '',
    date_time: '',
    technician_id: '',
    reason: '',
  });

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    fetch('http://localhost:8080/api/appointments/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        date_time: formData.date_time,
        reason: formData.reason,
        status: 'Scheduled',
        vin: formData.vin,
        customer: formData.customer,
        technician_id: formData.technician_id,
      }),
    })
      .then(response => response.json())
      .then(data => {
        console.log(data);
        alert('Service appointment created successfully!');
      })
      .catch(error => {
        console.error(error);
        alert('Failed to create service appointment. Please try again.');
      });
  };

  return (
    <div>
      <h1>Create Service Appointment</h1>
      <form onSubmit={handleSubmit}>
        <label htmlFor="vin">VIN:</label>
        <input
          type="text"
          id="vin"
          name="vin"
          value={formData.vin}
          onChange={handleChange}
          required
        />

        <label htmlFor="customer">Customer Name:</label>
        <input
          type="text"
          id="customer"
          name="customer"
          value={formData.customer}
          onChange={handleChange}
          required
        />

        <label htmlFor="date_time">Date and Time:</label>
        <input
          type="datetime-local"
          id="date_time"
          name="date_time"
          value={formData.date_time}
          onChange={handleChange}
          required
        />

        <label htmlFor="technician_id">Technician ID:</label>
        <input
          type="text"
          id="technician_id"
          name="technician_id"
          value={formData.technician_id}
          onChange={handleChange}
          required
        />

        <label htmlFor="reason">Reason:</label>
        <input
          type="text"
          id="reason"
          name="reason"
          value={formData.reason}
          onChange={handleChange}
          required
        />

        <button type="submit">Create Service Appointment</button>
      </form>
    </div>
  );
};

export default ServiceAppointmentForm;
