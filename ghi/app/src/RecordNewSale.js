import React, { useState, useEffect } from 'react';

const SalespeopleList = () => {
  const [salespeople, setSalespeople] = useState([]);

  useEffect(() => {
    const fetchSalespeople = async () => {
      try {
        const response = await fetch('http://localhost:3000/api/salespeople');
        if (!response.ok) {
          throw new Error(`HTTP error! status: ${response.status}`);
        }
        const data = await response.json();
        setSalespeople(data.salespeople); 
      } catch (error) {
        console.error(`Could not get salespeople: ${error}`);
      }
    };

    fetchSalespeople();
  }, []);

  return (
    <div className="container mt-4">
      <h1>Salespeople</h1>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">Employee ID</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
          </tr>
        </thead>
        <tbody>
          {salespeople.map(salesperson => (
            <tr key={salesperson.employee_id}>
              <td>{salesperson.employee_id}</td>
              <td>{salesperson.first_name}</td>
              <td>{salesperson.last_name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default SalespeopleList;
