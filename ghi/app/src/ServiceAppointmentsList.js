import React, { useState, useEffect } from 'react';

const ServiceAppointmentsList = () => {
  const [appointments, setAppointments] = useState([]);

  useEffect(() => {
    fetch('http://localhost:8080/api/appointments/')
      .then(response => response.json())
      .then(data => setAppointments(data))
      .catch(error => console.error(error));
  }, []);

  const handleCancel = (id) => {
    fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then(data => {
        const updatedAppointments = appointments.map(app => (app.id === id ? data : app));
        setAppointments(updatedAppointments.filter(app => app.status !== 'Canceled'));
      })
      .catch(error => {
        console.error(error);
      });
  };

  const handleFinish = (id) => {
    fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then(data => {
        const updatedAppointments = appointments.map(app => (app.id === id ? data : app));
        setAppointments(updatedAppointments.filter(app => app.status !== 'Finished'));
      })
      .catch(error => {
        console.error(error);
      });
  };

  return (
    <div>
      <h1>Service Appointments List</h1>
      <ul>
        {appointments.map(app => (
          <li key={app.id}>
            <strong>{app.customer}</strong> - {app.vin} - {app.date_time} - {app.technician_id} - {app.reason} - {app.status}
            <button onClick={() => handleCancel(app.id)}>Cancel</button>
            <button onClick={() => handleFinish(app.id)}>Finish</button>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default ServiceAppointmentsList;
